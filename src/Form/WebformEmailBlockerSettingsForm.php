<?php

namespace Drupal\webform_email_blocker\Form;

use Drupal\Core\Form\{ConfigFormBase, FormStateInterface};
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure site information settings for this site.
 */
class WebformEmailBlockerSettingsForm extends ConfigFormBase {

    /**
     * The config factory.
     *
     * @var ConfigFactoryInterface
     */
    protected $configFactory;
    /**
     * Constructs a new SettingsForm object.
     *
     * @param ConfigFactoryInterface $config_factory
     *   The factory for configuration objects.
     */
    public function __construct(ConfigFactoryInterface $config_factory) {
        $this->configFactory = $config_factory;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('config.factory'),
        );
    }
    /**
     * @return string
     */
    public function getFormId(): string
    {
        return "webform_email_blocker_settings";
    }

    /**
     * @return array
     */
    public function getEditableConfigNames(): array
    {
        return [
            "webform_email_blocker.settings",
        ];
    }

    /**
     * @param array $form
     * @param FormStateInterface|null $form_state
     * @return array
     */
    public function buildForm(array $form, ?FormStateInterface $form_state): array
    {
        $config = $this->configFactory->get("webform_email_blocker.settings");

        $form['webform_email_blocker'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Webform Email Blocker settings'),
        ];

        $form['webform_email_blocker']['description'] = [
            '#markup' => '<p>Settings for Webform Email Blocker</p>',
        ];

        $form['webform_email_blocker']['blocked_error_message'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Error Message Text'),
            '#default_value' => $config->get('blocked_error_message'),
            '#required' => true,
        ];

        $form['webform_email_blocker']['blocked_emails'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Blocked Email List'),
            '#description' => $this->t('Enter emails in a comma separated list.'),
            '#default_value' => $config->get('blocked_emails'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, ?FormStateInterface $formState)
    {
        $values = $formState->getValues();

        $this->configFactory->getEditable('webform_email_blocker.settings')
            ->set('blocked_emails', $values['blocked_emails'])
            ->set('blocked_error_message', $values['blocked_error_message'])
            ->save();

        parent::submitForm($form, $formState);
    }

}
