<?php

namespace Drupal\webform_email_blocker\Plugin\WebformHandler;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform validate handler.
 *
 * @WebformHandler(
 *   id = "webform_email_blocker_validator",
 *   label = @Translation("Webform Email Blocker"),
 *   category = @Translation("Settings"),
 *   description = @Translation("Allows Webform to block submissions for emails."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class EmailBlockerWebformHandler extends WebformHandlerBase
{

    /**
     * The token manager.
     *
     * @var \Drupal\webform\WebformTokenManagerInterface
     */
    protected $tokenManager;

    /**
     * {@inheritdoc}
     */
    public static function create(
        ContainerInterface $container,
        array $configuration,
        $plugin_id,
        $plugin_definition
    ): EmailBlockerWebformHandler|WebformHandlerBase|ContainerFactoryPluginInterface {
        $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
        $instance->tokenManager = $container->get('webform.token_manager');
        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['webform_email_blocker'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Webform Email Blocker settings'),
        ];

        $form['webform_email_blocker']['description'] = [
        '#markup' => '<p>Enter the machine name of the email field to be validated. </p>',
        ];

        $form['webform_email_blocker']['email_field'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Email Field'),
        '#required' => true,
        '#default_value' => $this->configuration['email_field'] ?? '',
        ];

        return $this->setSettingsParents($form);
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void
    {
        parent::submitConfigurationForm($form, $form_state);
        $this->configuration['email_field'] =
        $form_state->getValue(['webform_email_blocker', 'email_field']);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(
        array &$form,
        FormStateInterface $form_state,
        WebformSubmissionInterface $webform_submission
    ) {
        // Get module configuration.
        $config = \Drupal::config('webform_email_blocker.settings');

        // Get Submitted Email Address.
        $emailAddress = $form_state->getValue($this->configuration['email_field']);

        // Get list of banned email addresses.
        $bannedEmails = array_map('trim', explode(',',
          strtolower($config->get('blocked_emails'))));

        if ($emailAddress && is_array($bannedEmails) && !empty($bannedEmails)) {
          // Check array by comparing an all lowercase array and value
          if (in_array(strtolower($emailAddress), $bannedEmails)) {
                $form_state->setErrorByName(
                    $this->configuration['email_field'],
                    $config->get('blocked_error_message')
                );
            }
        }
    }
}
