CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Maintainers

VERSION
--------
Current Verson 1.0


INTRODUCTION
------------

The Login Switch is a module that modifies Drupal's core user.login,
user.register, user.password routes by changing the path to a custom path.


REQUIREMENTS
------------

This module requires the Drupal Webform module version 6 or higher.


INSTALLATION
------------

Recommended installation:  composer require drupal/webform_email_blocker.

Install the user webform_email_blocker module as you would normally
install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

There is a configuration page located at /admin/structure/webform/email-blocker.

Enter a list of emails (1 per line) that you would like to blacklist from
submitting or sending emails from.

TROUBLESHOOTING
-----------


MAINTAINERS
-----------

Currently maintained by:
* Matthew Sherman
